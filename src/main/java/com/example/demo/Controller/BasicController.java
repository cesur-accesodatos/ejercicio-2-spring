package com.example.demo.Controller;

import com.example.demo.Model.Movie;
import com.example.demo.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

//@RestController
@Controller
public class BasicController {
    @Autowired
    private MovieService movieService;

    @RequestMapping("/")
    String hola() {
        return "bienvenida";
    }


    @RequestMapping("/pelis")
    String repositorio(Model model){
        List<Movie> pelis=this.movieService.getAllMovies();
        List<String>listaDeUrls=new ArrayList<>();
        List<String>listaDeNombres=new ArrayList<>();
        List<Long>listaDeID=new ArrayList<>();
        for (int i =0;i<pelis.size();i++){
            listaDeUrls.add(pelis.get(i).getUrl());
            listaDeNombres.add(pelis.get(i).getMovie_name());
            listaDeID.add(pelis.get(i).getId());
        }
        model.addAttribute("url",listaDeUrls);
        model.addAttribute("nombre",listaDeNombres);
        model.addAttribute("id",listaDeID);
        return "repositorio";
    }

    @RequestMapping("/addMovie")
    String add(Model model) {
        List<Movie> pelis=this.movieService.getAllMovies();
        List<String>listaDeUrls=new ArrayList<>();
        List<String>listaDeNombres=new ArrayList<>();
        List<Long>listaDeID=new ArrayList<>();
        for (int i =0;i<pelis.size();i++){
            listaDeUrls.add(pelis.get(i).getUrl());
            listaDeNombres.add(pelis.get(i).getMovie_name());
            listaDeID.add(pelis.get(i).getId());
        }
        model.addAttribute("url",listaDeUrls);
        model.addAttribute("nombre",listaDeNombres);
        model.addAttribute("id",listaDeID);
        return "addMovie";
    }
    @PostMapping("/addMovie")
    String add(@RequestParam String movie_name, @RequestParam String movie_url, Model model){
        Movie m=new Movie();
        m.setMovie_name(movie_name);
        m.setUrl(movie_url);
        this.movieService.addMovie(m);
        model.addAttribute("message", "La película '" + movie_name + "' ha sido añadida.");
        return "index";
    }

    @RequestMapping("/deleteMovie")
    String delete(Model model) {
        List<Movie> pelis=this.movieService.getAllMovies();
        List<String>listaDeUrls=new ArrayList<>();
        List<String>listaDeNombres=new ArrayList<>();
        List<Long>listaDeID=new ArrayList<>();
        for (int i =0;i<pelis.size();i++){
            listaDeUrls.add(pelis.get(i).getUrl());
            listaDeNombres.add(pelis.get(i).getMovie_name());
            listaDeID.add(pelis.get(i).getId());
        }
        model.addAttribute("url",listaDeUrls);
        model.addAttribute("nombre",listaDeNombres);
        model.addAttribute("id",listaDeID);
        return "deleteMovie";
    }
    @DeleteMapping("/deleteMovie")
    String delete(@RequestParam Long id, Model model) {
        Movie a=this.movieService.getMovieById(id);
        if(a==null){
            model.addAttribute("message", "La película no existe con ese id");
            return "index";
        }else{
            this.movieService.deleteMovie(id);
            model.addAttribute("message", "La película '" + a.getMovie_name() + "' ha sido eliminada.");
            return "index";
        }
    }

    @RequestMapping("/modifyMovie")
    String modify(Model model){
        List<Movie> pelis=this.movieService.getAllMovies();
        List<String>listaDeUrls=new ArrayList<>();
        List<String>listaDeNombres=new ArrayList<>();
        List<Long>listaDeID=new ArrayList<>();
        for (int i =0;i<pelis.size();i++){
            listaDeUrls.add(pelis.get(i).getUrl());
            listaDeNombres.add(pelis.get(i).getMovie_name());
            listaDeID.add(pelis.get(i).getId());
        }
        model.addAttribute("url",listaDeUrls);
        model.addAttribute("nombre",listaDeNombres);
        model.addAttribute("id",listaDeID);
        return "modifyMovie";
    }
    @PutMapping ("/modifyMovie")
    String modify(@RequestParam Long id, @RequestParam String url, @RequestParam String movie_name, Model model){
        Movie a = this.movieService.getMovieById(id);
        if(a == null){
            model.addAttribute("message", "La película no existe con ese id.");
            return "index";
        }else{
            if(url.isEmpty()){
                a.setUrl(a.getUrl());
            }else{
                a.setUrl(url);
            }
            if(movie_name.isEmpty()){
                a.setMovie_name(a.getMovie_name());
            }else{
                a.setMovie_name(movie_name);
            }
            this.movieService.updateMovie(id,a);
            model.addAttribute("message", "La película '" + a.getMovie_name() + "' ha sido actualizado.");
            return "index";
        }
    }

}

